package com.nirajan.springannotationdemo;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BaseBallConfig {
	
	@Bean
	public FortuneService veryLuckyFortuneService() {
		return new VeryLuckyFortuneService();
	}
	
	@Bean
	public Coach baseBallCoach() {
		return new BaseBallCoach(veryLuckyFortuneService());
	}

}
