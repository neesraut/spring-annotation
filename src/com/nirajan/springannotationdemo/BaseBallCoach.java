package com.nirajan.springannotationdemo;

public class BaseBallCoach implements Coach {

	 private FortuneService theFortuneService;
	 public BaseBallCoach(FortuneService fortuneService) {
		theFortuneService = fortuneService;
	}
	
	@Override
	public String getDailyWorkout() {
		return "Do 1000 times sit ups";
	}

	@Override
	public String getDailyFortune() {
		return theFortuneService.getFortune();
	}

}
