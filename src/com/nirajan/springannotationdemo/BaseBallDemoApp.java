package com.nirajan.springannotationdemo;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class BaseBallDemoApp {

	public static void main(String[] args) {
		//load the config File
		
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(BaseBallConfig.class);

		//load the bean
		Coach theCoach = context.getBean("baseBallCoach",Coach.class);
		
		//load the methods
		
	  System.out.println(theCoach.getDailyFortune());
	  System.out.println(theCoach.getDailyWorkout());
	
	  //close the context
	  context.close();
	}

}
