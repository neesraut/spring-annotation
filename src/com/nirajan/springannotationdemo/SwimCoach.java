package com.nirajan.springannotationdemo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component

public class SwimCoach implements Coach {

	@Autowired
	@Qualifier("sadFortuneService")
	private FortuneService fs;
	
	@Value("${foo.email}")
	String email;
	
	@Value("${foo.name}")
	String name;
	
	@Override
	public String getDailyWorkout() {
		// TODO Auto-generated method stub
		return "Daily workout from swim coach";
	}

	@Override
	public String getDailyFortune() {
		// TODO Auto-generated method stub
		return fs.getFortune();
	}
	
	
	public String getPropertiesValue() {
		return "name is:" +name +"email: "+email;
		
	}

}
