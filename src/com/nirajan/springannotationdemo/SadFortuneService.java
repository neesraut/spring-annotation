package com.nirajan.springannotationdemo;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.springframework.stereotype.Component;

@Component

public class SadFortuneService implements FortuneService {

	@Override
	public String getFortune() {
		
	  List<String> data = new ArrayList<String>();

		try {
			File  fr=new File("E:\\myfile.txt");
			BufferedReader br = new BufferedReader(new FileReader(fr));
			String str=null;
			while((str=br.readLine())!=null)
			{
				data.add(str);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("data from file: " +data);
		Random rn = new Random();
		int index = rn.nextInt(data.size());
		String fortune = data.get(index);
		
		return fortune;
	}

}
