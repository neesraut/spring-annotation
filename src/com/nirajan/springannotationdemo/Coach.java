package com.nirajan.springannotationdemo;

public interface Coach {
	
	public String getDailyWorkout();
	public String getDailyFortune();
	

}
