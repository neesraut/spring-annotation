package com.nirajan.springannotationdemo;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

//step 2 add @component in class
//@Component("thatSillyCoach")

@Component() // spring gives default bean id i.e. lowercase starting class name i.e
//@Scope("prototype")			// tennisCoach
public class TennisCoach implements Coach {

	@Autowired
	@Qualifier("randomFortuneService")
	private FortuneService fortuneService;

	/*
	 * //@Autowired public TennisCoach(FortuneService theFortuneService) {
	 * this.fortuneService = theFortuneService; }
	 */
	// default constructor
	
	  public TennisCoach() {
	  System.out.println("inside tennish coach default constructor"); 
	  }
	 

	// setter method for dependency injection
	
	//@Autowired //looks for the dependency and gives of object of class that implements fortuneservice automatically
	
	
	/*
	 * public void doSomeCrazyStuff(FortuneService fortuneService) {
	 * System.out.println("Inside tennish coach setter method"); this.fortuneService
	 * = fortuneService; }
	 */

	@Override
	public String getDailyWorkout() {
		return "Practice more your Backhand Volley";
	}

	@Override
	public String getDailyFortune() {
		// TODO Auto-generated method stub
		return fortuneService.getFortune();
	}
	
	//define init Method
	@PostConstruct
	public void doMyStartupStuff() {
		System.out.println(">> Tennish coach ; Inside starting method i.e. post construct");
	}
	
	@PreDestroy
	public void doCleanupMyStuff() {
		System.out.println("\nInside pre destroy---- tennish coach");
	}
	

}
