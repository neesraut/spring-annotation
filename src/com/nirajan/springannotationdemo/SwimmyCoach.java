package com.nirajan.springannotationdemo;

import org.springframework.beans.factory.annotation.Value;

public class SwimmyCoach implements Coach {

	private FortuneService fortuneService;
	
	public SwimmyCoach(FortuneService theFortuneService) {
		fortuneService = theFortuneService;
	
	}
	
	@Value("${foo.email}")
	private String email;
	
	public String getEmail() {
		return email;
	}

	public String getName() {
		return name;
	}

	@Value("${foo.email}")
	private String name;
	
	@Override
	public String getDailyWorkout() {
		return "Swim 1000 meters as a warm up";
	}

	@Override
	public String getDailyFortune() {
		return fortuneService.getFortune();
	}

}
