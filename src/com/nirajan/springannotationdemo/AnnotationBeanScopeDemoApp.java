package com.nirajan.springannotationdemo;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class AnnotationBeanScopeDemoApp {

	public static void main(String[] args) {
		// load spring config file
		
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		
		//retrieve bean from spring container
		Coach theCoach = context.getBean("tennisCoach",Coach.class);
		
		//Coach alphaCoach = context.getBean("tennisCoach",Coach.class);
		
		
		/*
		 * boolean result = (theCoach==alphaCoach);
		 * System.out.println("\n pointing to same memory: "+ result);
		 * 
		 * System.out.println("\n Memory location of thecaoch: "+ theCoach);
		 * 
		 * System.out.println("\n Memory location of alphaCoach: "+ alphaCoach);
		 */
		context.close();
	}

}
