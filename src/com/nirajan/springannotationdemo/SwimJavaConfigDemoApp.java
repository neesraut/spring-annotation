package com.nirajan.springannotationdemo;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class SwimJavaConfigDemoApp {

	public static void main(String[] args) {

		// read the config file
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(SportConfig.class);

		// get the bean from spring container
		
		SwimmyCoach theCoach = context.getBean("swimsCoach", SwimmyCoach.class);
		
		// call a method on the bean

		//Coach c = context.getBean("cricketCoach",Coach.class);
		//System.out.println(c.getDailyWorkout());
		
		//call the method to get daily fortune
		System.out.println(theCoach.getDailyWorkout());
		
		System.out.println(theCoach.getDailyFortune());
		
		System.out.println("Email: "+theCoach.getEmail());
		
		System.out.println("name: "+theCoach.getName());
		
		// close the context
		context.close();
	}

}
