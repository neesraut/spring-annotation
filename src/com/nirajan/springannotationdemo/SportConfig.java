package com.nirajan.springannotationdemo;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

//step 1--
@Configuration
//@ComponentScan("com.nirajan.springannotationdemo")

@PropertySource("classpath:sport.properties")
public class SportConfig {
	
	//define bean for our sad fortune service
	@Bean
	public FortuneService sadFortuneService() {
		return new SaddyFortuneService();
	}
	
	//define bean for our swim coach and inject dependency
	@Bean
	public Coach swimsCoach() {
		return new SwimmyCoach(sadFortuneService());
		
	}
}
